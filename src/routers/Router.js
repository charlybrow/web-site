// Import dependencies.
import React, { Component } from "react";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
// Import components.
import Header from "../components/header/Header";
import Body from "../components/body/Body";
import Footer from "../components/footer/Footer";
import Authorization from "../components/body/authorization/Authorization";
import ContactUs from "../components/body/contacus/ContactUs";
import ForgotPassword from "../components/body/password/Forgot";
import GCRegistration from "../components/body/register/RegisterGC";
import RequestDemo from "../components/body/demo/RequestDemo";
import ResetPassword from "../components/body/password/Reset";
import SetPassword from "../components/body/password/Set";
import SubRegistration from "../components/body/register/RegisterSub";
import Verification from "../components/body/verification/Verification";

// Class router.
class Router extends Component {
   //
   render() {
      // Safe return.
      return(
         <BrowserRouter>
            <div>
               <Header />
               <Switch>
                  <Route exact path="/" render={ () => {
                     // Validate location.
                     if ( window.location.pathname === "/" ) {
                        // Redirect to Home.
                        return <Redirect to="/Home" />;
                     }
                  }}/>
                  <Route exact path="/Home" component={Body} />
                  <Route exact path="/Authorization" component={Authorization} />
                  <Route exact path="/ContactUs" component={ContactUs} />
                  <Route exact path="/ForgotPassword" component={ForgotPassword} />
                  <Route exact path="/GCRegistration" component={GCRegistration} />
                  <Route exact path="/RequestDemo" component={RequestDemo} />
                  <Route exact path="/ResetPassword" component={ResetPassword} />
                  <Route exact path="/SetPassword" component={SetPassword} />
                  <Route exact path="/SubRegistration" component={SubRegistration} />
                  <Route exact path="/Verification" component={Verification} />
               </Switch>
               <Footer />
            </div>
         </BrowserRouter>
      )
   }
};

// Export router.
export default Router;
