// Import types.
import {
   REQUEST_INFO,
   CONTACT_US,
   FORGOT_PASSWORD,
   RESET_PASSWORD,
   SET_PASSWORD, AUTHORIZE,
   VALIDATE,
   AUTHENTICATE,
   REGISTER_SUB,
   REGISTER_GC,
   FETCH_TRADE_LIST,
   FETCH_MINORITY_LIST,
   FETCH_COMPANY_KEYWORD_LIMIT,
   FETCH_TRADE_LIST_KEYWORD_VERTICAL,
   VALIDATE_RECAPTCHA,
   FETCH_TRADE_LIST_CODES
} from "./types";

// Request info.
const requestInfo = data => {
   // Safe return.
   return {
      type: REQUEST_INFO,
      payload: data
   }
};

// Contact us.
const contactUs = data => {
   // Safe return.
   return {
      type: CONTACT_US,
      payload: data
   }
};

// Forgot password.
const forgotPassword = data => {
   // Safe return.
   return {
      type: FORGOT_PASSWORD,
      payload: data
   }
};

// Reset password.
const resetPassword = data => {
   // Safe return.
   return {
      type: RESET_PASSWORD,
      payload: data
   }
};

// Set password.
const setPassword = data => {
   // Safe return.
   return {
      type: SET_PASSWORD,
      payload: data
   }
};

// Authorize.
const authorize = data => {
   // Safe return.
   return {
      type: AUTHORIZE,
      payload: data
   }
};

// Validate.
const validate = data => {
   // Safe return.
   return {
      type: VALIDATE,
      payload: data
   }
};

// Authenticate.
const authenticate = data => {
   // Safe return.
   return {
      type: AUTHENTICATE,
      payload: data
   }
};

// Sub Registration.
const registerSub = data => {
   // Safe return.
   return {
      type: REGISTER_SUB,
      payload: data
   }
};

// GC Registration.
const registerGC = data => {
   // Safe return.
   return {
      type: REGISTER_GC,
      payload: data
   }
};

// Get trade list.
const getTradeList = data => {
   // Safe return.
   return {
      type: FETCH_TRADE_LIST,
      payload: data
   }
};

// Get minority list
const getMinorityist = data => {
   // Safe return.
   return {
      type: FETCH_MINORITY_LIST,
      payload: data
   }
};

// Get Company Keword Limit.
const getCompanyKeywordLimit = data => {
   // Safe return.
   return {
      type: FETCH_COMPANY_KEYWORD_LIMIT,
      payload: data
   }
};

// Get Trade List Keywords Vertical.
const getTradeListKeywordsVertical = data => {
   // Safe return.
   return {
      type: FETCH_TRADE_LIST_KEYWORD_VERTICAL,
      payload: data
   }
};

// Validate Recaptcha.
const validateRecaptcha = data => {
   // Safe return.
   return {
      type: VALIDATE_RECAPTCHA,
      payload: data
   }
};

// Get Trade Codes.
const getTradeListCodes = data => {
   // Safe return.
   return {
      type: FETCH_TRADE_LIST_CODES,
      payload: data
   }
};

// Export acctions.
export {
   requestInfo,
   contactUs,
   forgotPassword,
   resetPassword,
   setPassword,
   authorize,
   validate,
   authenticate,
   registerSub,
   registerGC,
   getTradeList,
   getMinorityist,
   getCompanyKeywordLimit,
   getTradeListKeywordsVertical,
   validateRecaptcha,
   getTradeListCodes
};
