// Import dependencies.
import React, { Component } from "react";

// Extend react components.
class Verification extends Component {
   // Render.
   render() {
      // Safe return.
      return(
         /* Main container. */
         <div style={{ padding: "20px 55px 0 55px"}}>
            <div className="mbottom30">
               <label><b className="black">Pizza House</b> Profile Update Peter Gc</label>
               <div className="mtop20 full-width bg-C9C9C9 no-padding app-divider-line"></div>
            </div>
            <div className="font18">
               <p className="mbottom30">Your profile changes have been submitted.</p>
               <p>You may now close this page.</p>
            </div>
         </div>
      );
   };
};

// Return Verification.
export default Verification;
