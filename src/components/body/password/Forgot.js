// Import dependencies.
import React, { Component } from "react";
// Import Images.
import logoBidmail from "../../../images/app/BidMail_Websitelogo.png";

// Exendes componente react.
class ForgotPassword extends Component {
   // Render.
   render() {
      // Safe return.
      return(
         <div className="flex text-flex-center content-column" style={{height: "609px"}}>
            {/* Header title */}
            <div className="flex text-flex-center mtop20 mbottom20">
               <img className="mright20" src={logoBidmail} alt="BM Share" />
               <span className="font25 gray-828282">Login</span>
            </div>
            {/* Card form.*/}
            <div className="flex text-flex-center content-column bg-gray-F2F2F2" style={{padding: "0 152px 60px 152px"}}>
               {/* Title */}
               <label className="font20 mtop20 mbottom20">Forgot Password?</label>
               {/* Body */}
               <div className="flex content-column bg-white border border-CCC black" style={{padding: "0 80px 0 80px"}}>
                  <p className="font12 mtop25 mbottom25">Enter Email address:</p>
                  {/* Email Address. */}
                  <div className="block app-text-field pleft10 pright10 mbottom20" style={{width: "315px"}}>
                     <input className="block2 full-width full-height" type="text" placeholder="Email address" />
                  </div>
                  {/* Confirm Email Address */}
                  <div className="block app-text-field pleft10 pright10" style={{width: "315px", marginBottom: "50px"}}>
                     <input className="block2 full-width full-height" type="text" placeholder="Confirm Email address" />
                  </div>
                  <div className="text-right mbottom20">
                     <button className="mright15 size-100x30 bg-gray" style={{borderRadius: 4 }}>Back</button>
                     <button className="size-100x30 bg-blue1" style={{borderRadius: 4 }}>Submit</button>
                  </div>
               </div>
            </div>
         </div>
      )
   }
}

export default ForgotPassword;
