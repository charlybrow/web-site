// Import react.
import React, { Component } from "react";
// Import Images.
import checkMark from "../../../images/home/checkmark.png";
// Import css.
import "../../../style/home/home.css";

// Class react component.
class GCRegistration extends Component {
   //
   constructor(props) {
      //
      super(props);
      //
      this.state = {
         features: [
            {
              id: 1,
              description: "Access to Subs in the BidMail Network"
            },
            {
              id: 2,
              description: "Free for the first user"
            },
            {
              id: 3,
              description: "Trusted by many General Contractors"
            },
            {
              id: 4,
              description: "Viewer displaying project files with indexing"
            },
            {
              id: 5,
              description: "Private database of your own subcontractors"
            },
            {
              id: 6,
              description: "Top notch customer service and support"
            },
            {
              id: 7,
              description: "Muchs more..."
            }
         ]
      };
   }

   // Render.
   render() {
      // Get features.
      const { features } = this.state;
      //
      return(
         /* Main container*/
         <div className="flex">
            {/* Features */}
            <div className="started-gc flex text-flex-center content-right">
               {/* List features */}
               <div className="mright50">
                  <ul>
                     {
                        features.map((feature) => (
                           <li key={feature.id} className="mbottom20">
                              <img className="mright20 text-middle" style={{width: "20px", height: "20px"}} src={checkMark} alt="Checkmark" />
                              <span className="font13 white">{feature.description}</span>
                           </li>
                        ))
                     }
                  </ul>
               </div>
            </div>
            {/* Form */}
            <div className="" style={{marginLeft: "150px"}}>
               <div className="mbottom20 mtop40">
                  <span className="font24 gray">Get Started</span>
               </div>
               {/* Company Name */}
               <label className="font12 font-bold">Company Name</label>
               <div className="app-text-field pleft10 pright10 mbottom20" style={{width: "280px"}}>
                  <input className="block full-width full-height" type="text" />
               </div>
               {/* Company Phone*/}
               <label className="font12 font-bold">Company Phone</label>
               <div className="app-text-field pleft10 pright10 mbottom20" style={{width: "280px"}}>
                  <input className="block full-width full-height" type="text" />
               </div>
               <div className="flex space-between">
                  {/* First Name*/}
                  <div>
                     <label className="font12 font-bold">First Name</label>
                     <div className="app-text-field pleft10 pright10 mbottom20" style={{width: "125px"}}>
                        <input className="block full-width full-height" type="text" />
                     </div>
                  </div>
                  {/* Last Name*/}
                  <div>
                     <label className="font12 font-bold">Last Name</label>
                     <div className="app-text-field pleft10 pright10 mbottom20" style={{width: "125px"}}>
                        <input className="block full-width full-height" type="text" />
                     </div>
                  </div>
               </div>
               {/* Working Email*/}
               <label className="font12 font-bold">Working Email</label>
               <div className="app-text-field pleft10 pright10 mbottom20" style={{width: "280px"}}>
                  <input className="block full-width full-height" type="text" />
               </div>
               {/* Recaptcha */}
               <div style={{width: "304px", height: "78px"}}>Recaptcha</div>
               {/* Button */}
               <button className="font11 white no-border bg-blue-0099FF mleft-auto mright-auto block" style={{width: "110px", height: "25px"}}>Continue</button>
               {/* Services*/}
               <div className="mtop25 font9">
                  <span>By clicking "Continue" I agree to BidMail´s </span>
                  <span className="blue-09F pointer link-underline">Terms of Use </span>
                  <span>and </span>
                  <span className="blue-09F pointer link-underline">Privacy Policy</span>
               </div>
            </div>
         </div>
      )
   };
};

// Safe return.
export default GCRegistration;
