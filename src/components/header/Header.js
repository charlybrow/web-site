// Import dependencies.
import React, { Component } from "react";
import { Link } from 'react-router-dom';
// Import Images.
import logoBidmail from "../../images/header/BidMail_Website-Logo_color.png";
// Import css.
import "../../style/header/Header.css";
// Import Components.
import Login from "./login/Login";

// Extends React component.
class Header extends Component {
   // Render.
   render() {
      // Safe return.
      return(
         <header className="header flex content-center text-flex-center black font14">
            <div className="mright40 pointer">
               <img src={logoBidmail} alt="logo" />
            </div>
            <div className="mright30">
               <Link to={'/Verification'}>
                  <label className="pointer">Features</label>
               </Link>
            </div>
            <div className="mright30">
               <label className="pointer">Pricing</label>
            </div>
            <div className="mright30">
               <label className="pointer">Store</label>
            </div>
            <div className="mright30">
               <label className="pointer">Contacts</label>
            </div>
            <div className="mright50">
               <label className="pointer">Tutorials</label>
            </div>
            <Login />
         </header>
      )
   };
};

// Return Header.
export default Header;
