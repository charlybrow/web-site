// Import dependencies.
import React, { Component } from "react";
// Redux
import { connect } from 'react-redux';
//
import { authenticate } from "../../../actions/actions";

// Extends React component.
class Login extends Component {
   //
   constructor(props) {
      // Inherit from reac base.
      super(props);

      // State.
      this.state = {
         userName: "",
         password: ""
      };
   };

   // Update user name.
   username = e => {
      // Update state.
      this.setState({"userName": e.target.value});
   };

   // Update password.
   password = e => {
      // Update state.
      this.setState({"password": e.target.value});
   };

   // Authentificate login.
   authentificateLogin = e => {
      // Prevent default.
      e.preventDefault();
      // Get user name and password.
      const { userName, password } = this.state;

      // Crear el objeto
      const userInfo = {
         userName,
         password
      }

      // Create authenticate.
      this.props.authenticate(userInfo);
   };

   // Render.
   render() {
      // Safe return.
      return(
         <div>
            <form onSubmit={this.authentificateLogin}>
               {/* User name */}
               <div className="inline-block app-text-field pright10 pleft10 mright10 bg-white">
                  <input className="block full-width full-height" placeholder="Email" type="text" onChange={this.username} />
               </div>
               {/* Password */}
               <div className="inline-block app-text-field pright10 pleft10 mright20 bg-white">
                  <input className="block full-width full-height" placeholder="Password" type="password" onChange={this.password}/>
               </div>
               {/* Button */}
               <button type="submit">Sign In</button>
            </form>
         </div>
      )
   }
}

// Return Login component.
export default connect(null, {authenticate})(Login);
