// Import types.
import {
   FETCH_TRADE_LIST,
   FETCH_MINORITY_LIST,
   FETCH_COMPANY_KEYWORD_LIMIT,
   FETCH_TRADE_LIST_KEYWORD_VERTICAL,
   FETCH_TRADE_LIST_CODES
} from "../actions/types";

// Init state.
const initialState = {
   tradeList: {},
   minorityList: {},
   tradeListKeyword: {},
   tradeListCode: {},
   companyKeyworldLimit: {}
};

// Export.
export default function (state = initialState, action) {
    // Validate events.
   switch(action.type) {
      case FETCH_TRADE_LIST:
         return {
            ...state,
            tradeList: action.payload
         }
      case FETCH_MINORITY_LIST:
         return {
            ...state,
            minorityList: action.payload
         }
      case FETCH_COMPANY_KEYWORD_LIMIT:
         return {
            ...state,
            companyKeyworldLimit: action.payload
         }
      case FETCH_TRADE_LIST_KEYWORD_VERTICAL:
         return {
            ...state,
            tradeListKeyword: action.payload
         }
      case FETCH_TRADE_LIST_CODES:
         return {
            ...state,
            tradeListCode: action.payload
         }
      default:
         return state;
   };
};

