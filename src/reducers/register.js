// Import types.
import { REGISTER_SUB, REGISTER_GC } from "../actions/types"

// Init state.
const initialState = {
   register: []
};

// Export
export default function (state = initialState, action) {
   // Validate events.
   switch(action.type) {
      case REGISTER_SUB:
         return {
            ...state,
            register: action.payload
         }
      case REGISTER_GC:
         return {
            ...state,
            register: action.payload
         }
      default:
         return state;
   };
};
