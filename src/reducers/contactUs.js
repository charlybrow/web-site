// Import types.
import { REQUEST_INFO, CONTACT_US } from "../actions/types"

// Init state.
const initialState = {
   userInfo: {},
   contactInfo: {}
};

// Export
export default function (state = initialState, action) {
   // Validate events.
   switch(action.type) {
      case REQUEST_INFO:
         return {
            ...state,
            userInfo: action.payload
         }
      case CONTACT_US:
         return {
            ...state,
            contactInfo: action.payload
         }
      default:
         return state;
   };
};

