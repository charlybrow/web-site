// Import dependencies.
import { combineReducers } from 'redux';
import contactUs from './contactUs';
import register from './register';
import security from './security';
import catalogo from './catalogs'

// Expor comnine.
export default combineReducers({
   contactUs,
   register,
   security,
   catalogo
});
