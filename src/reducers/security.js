// Import types.
import {
   FORGOT_PASSWORD,
   RESET_PASSWORD,
   SET_PASSWORD,
   AUTHORIZE,
   VALIDATE,
   AUTHENTICATE,
   VALIDATE_RECAPTCHA
} from "../actions/types";

// Init state.
const initialState = {
   forgoPassword: {},
   resetPassword: {},
   setPassword: {},
   authorize: {},
   validate: {},
   authenticate: {},
   capcha: {}
};

// Export.
export default function (state = initialState, action) {
   console.log(action)
   // Validate events.
   switch(action.type) {
      case FORGOT_PASSWORD:
         return {
            ...state,
            forgoPassword: action.payload
         }
      case RESET_PASSWORD:
         return {
            ...state,
            resetPassword: action.payload
         }
      case SET_PASSWORD:
         return {
            ...state,
            setPassword: action.payload
         }
      case AUTHORIZE:
         return {
            ...state,
            authorize: action.payload
         }
      case VALIDATE:
         return {
            ...state,
            validate: action.payload
         }
      case AUTHENTICATE:
         return {
            ...state,
            authenticate: action.payload
         }
      case VALIDATE_RECAPTCHA:
         return {
            ...state,
            capcha: action.payload
         }
      default:
         return state;
   };
};
