import React, { Component } from 'react';
// Redux.
import { Provider } from 'react-redux';
import store from './store/store';
import Router from "./routers/Router";

class App extends Component {
   render() {
      return (
         <Provider store={store} >
            <Router />
         </Provider>
      );
   }
}

export default App;
