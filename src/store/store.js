import { createStore } from 'redux';
import rootReducer from '../reducers';

// Create store.
const store = createStore(
   rootReducer,
   window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

// Export store.
export default store;
